/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.sukdituch.robotproject;

/**
 *
 * @author focus
 */
public class TestRobot {
    public static void main(String[] args) {
        Robot robot = new Robot(1, 2, 7, 10, 100);
        System.out.println(robot);
        
        robot.walk('N');
        System.out.println(robot);
        
        robot.walk('E');
        robot.walk('E');
        System.out.println(robot);
        
        robot.walk('E', 4);
        robot.walk('S', 5);
        System.out.println(robot);
        
        robot.walk();
        System.out.println(robot);
        robot.walk(3);
        System.out.println(robot);
    } 
}
